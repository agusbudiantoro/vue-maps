import Vue from 'vue'
import Router from 'vue-router'
import GPS from './MYGps.vue'
import App from '../App.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: App
    },
    {
      path: '/location',
      name: 'Location',
      component: GPS
    }
  ]
})
